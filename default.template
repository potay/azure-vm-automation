proxy_set_header               Host $host;
proxy_set_header               X-Real-IP $remote_addr;
proxy_set_header               X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header               X-Forwarded-Host $server_name;

server {
    listen                     443 ssl;
    listen                     [::]:443 ssl;
    server_name                ${HOMEPAGE_DOMAIN};
    include                    /etc/nginx/mime.types;
    gzip                       on;
    gzip_min_length            1000;
    gzip_proxied               expired no-cache no-store private auth;
    gzip_types                 text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    location / {
        proxy_pass             http://localhost:8080/;
        proxy_redirect         off;
        proxy_http_version     1.1;
        proxy_set_header       Upgrade $http_upgrade;
        proxy_set_header       Connection "upgrade";
    }
    error_page                 500 502 503 504  /50x.html;
    location = /50x.html {
        root                   html;
        internal;
    }
}

server {
    listen                     443 ssl;
    listen                     [::]:443 ssl;
    server_name                ${WEBSERVICE_DOMAIN};
    include                    /etc/nginx/mime.types;
    gzip                       on;
    gzip_min_length            1000;
    gzip_proxied               expired no-cache no-store private auth;
    gzip_types                 text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    location / {
        proxy_pass             http://localhost:8081/;
        proxy_redirect         off;
        proxy_http_version     1.1;
        proxy_set_header       Upgrade $http_upgrade;
        proxy_set_header       Connection "upgrade";
    }
    error_page                 500 502 503 504  /50x.html;
    location = /50x.html {
        root                   html;
        internal;
    }
}

server {
    listen                     443 ssl;
    listen                     [::]:443 ssl;
    server_name                ${PREDICTOR_DOMAIN};
    include                    /etc/nginx/mime.types;
    gzip                       on;
    gzip_min_length            1000;
    gzip_proxied               expired no-cache no-store private auth;
    gzip_types                 text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    location / {
        proxy_pass             http://localhost:8082/;
        proxy_redirect         off;
        proxy_http_version     1.1;
        proxy_set_header       Upgrade $http_upgrade;
        proxy_set_header       Connection "upgrade";
    }
    error_page                 500 502 503 504  /50x.html;
    location = /50x.html {
        root                   html;
        internal;
    }
}

server {
    listen                     443 ssl;
    listen                     [::]:443 ssl;
    server_name                ${RASPISAMPLE_DOMAIN};
    include                    /etc/nginx/mime.types;
    gzip                       on;
    gzip_min_length            1000;
    gzip_proxied               expired no-cache no-store private auth;
    gzip_types                 text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    location / {
        proxy_pass             http://localhost:8083/;
        proxy_redirect         off;
        proxy_http_version     1.1;
        proxy_set_header       Upgrade $http_upgrade;
        proxy_set_header       Connection "upgrade";
    }
    error_page                 500 502 503 504  /50x.html;
    location = /50x.html {
        root                   html;
        internal;
    }
}

server {
    listen 80;
    listen [::]:80;
    server_name ${HOMEPAGE_DOMAIN};

    if ($host = ${HOMEPAGE_DOMAIN}) {
        return 301 https://${HOMEPAGE_DOMAIN}$request_uri;
    }
    return 404;
}

server {
    listen 80 default_server;
    listen [::]:80 default_server;
    listen 443 ssl default_server;
    listen [::]:443 ssl default_server;
    server_name ${ROOT_DOMAIN};

    if ($host = ${ROOT_DOMAIN}) {
        return 301 https://${HOMEPAGE_DOMAIN}$request_uri;
    }
    return 404;
}
