#!/bin/bash
set -eu
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Install dependencies
nginx_exists=$(which nginx)
if [[ -z "${nginx_exists// }" ]]; then
    apt-get install nginx -y
fi
certbot_exists=$(which certbot)
if [[ -z "${certbot_exists// }" ]]; then
    apt-get install certbot python-certbot-nginx -y
fi
docker_exists=$(which docker)
if [[ -z "${docker_exists// }" ]]; then
    apt-get install apt-transport-https ca-certificates curl software-properties-common -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    apt-get install docker-ce -y
fi
docker_compose_exists=$(which docker-compose)
if [[ -z "${docker_compose_exists// }" ]]; then
    curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    usermod -aG docker ${VM_USER}
fi

# Install keys from Azure
secretsname=$(find /var/lib/waagent/ -name "*.prv" | cut -c -57)
mkdir -p /etc/nginx/ssl
cp $secretsname.crt /etc/nginx/ssl/potaycert.cert
cp $secretsname.prv /etc/nginx/ssl/potaycert.prv
echo "${SSL_FULLCHAIN_SIGNATURE}" >/etc/letsencrypt/live/${ROOT_DOMAIN}/fullchain.pem
echo "${SSL_PRIVATE_KEY}" >/etc/letsencrypt/live/${ROOT_DOMAIN}/privkey.pem

# Update NGINX configurations
cp $DIR/nginx.conf /etc/nginx/nginx.conf
chown www-data:www-data /etc/nginx/nginx.conf
cp $DIR/default.template /etc/nginx/sites-enabled/default
chown www-data:www-data /etc/nginx/sites-enabled/default
systemctl restart nginx
sudo certbot run --nginx -n --reinstall -d ${ROOT_DOMAIN}

# Add new service
cp $DIR/docker-compose.yml /home/${VM_USER}/docker-compose.yml
chown ${VM_USER}:${VM_USER} /home/${VM_USER}/docker-compose.yml
cp $DIR/dockerserver.service /etc/systemd/system/${SERVICE_NAME}.service

# Run service
docker login --username=${REGISTRY_USER} --password=${REGISTRY_PASSWORD} ${REGISTRY_DOMAIN}
docker-compose -f /home/${VM_USER}/docker-compose.yml pull
systemctl enable ${SERVICE_NAME}
systemctl restart ${SERVICE_NAME}
